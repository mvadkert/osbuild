#!/usr/bin/bash
# Execute osbuild unit tests from a checked out dist-git repo

set -euxo pipefail

source /etc/os-release

case "${ID}" in
    fedora)
        PKG_MAINT_TOOL="fedpkg"
        ;;
    rhel)
        PKG_MAINT_TOOL="rhpkg"
        ;;
    centos)
        PKG_MAINT_TOOL="centpkg"
        ;;
    *)
        echo "Error: unsupported OS: ${ID}-${VERSION_ID}"
        exit 1
        ;;
esac

# Move to the checked out git repo with the test plans
# this should be the root of the dist-git repo
cd "${TMT_TREE}"
# the content of the directory is copy of the dist-git repo, but the .git directory is missing
# so we need to create it to make all *pkg tools happy
git init

# deduct the release to pass to rpkg to prep the sources.
# 1. use the DIST_GIT_RELEASE if set
DIST_GIT_RELEASE="${DIST_GIT_RELEASE:-}"
# 2. use the TARGET_BRANCH if set
if [[ -z "${DIST_GIT_RELEASE}" ]]; then
    echo "DIST_GIT_RELEASE was not provided, trying to use TARGET_BRANCH"
    DIST_GIT_RELEASE="${TARGET_BRANCH:-}"
fi
# 3. use the host OS type and version
if [[ -z "${DIST_GIT_RELEASE}" ]]; then
    echo "DIST_GIT_RELEASE nor TARGET_BRANCH were not provided, trying to use the host OS type and version"
    case "${ID}" in
        fedora)
            DIST_GIT_RELEASE="f${VERSION_ID}"
            ;;
        rhel)
            DIST_GIT_RELEASE="rhel-${VERSION_ID}.0"
            ;;
        centos)
            DIST_GIT_RELEASE="c${VERSION_ID}s"
            ;;
    esac
fi

# The namespace and name must be passed explicitly to rhpkg, because it can't determine them on its own.
# Passing these options to centpkg and fedpkg is harmless, so we can use them for all tools.
# shellcheck disable=SC2086
${PKG_MAINT_TOOL} -v --namespace rpms --name osbuild --release "${DIST_GIT_RELEASE}" prep

# Extract the Source0 basename without extension
SRC_DIR=$(spectool --source 0 osbuild.spec | sed 's/.\+\(osbuild-[0-9]\+\)\.tar\.gz/\1/')

cd "${SRC_DIR}"

# Some unit tests depend on the fact that the source code is in a git repo
git init .
git add *
git commit -m "Initial commit"

# The 'test/run/test_assemblers.py::test_tar' test case uses system tar to extract the built tar archive.
# However, files in the tar archive can have SELinux context not present in the system policy. Therefore,
# in order for the system tar to be able to set it when extracting the archive, it must be labeled with
# 'install_exec_t' type.

ORIGINAL_TAR_CONTEXT="$(matchpathcon -n "$(which tar)")"
sudo chcon "system_u:object_r:install_exec_t:s0" "$(which tar)"
function restore_tar_context() {
    sudo chcon "${ORIGINAL_TAR_CONTEXT}" "$(which tar)"
}
trap restore_tar_context EXIT

# Run the unit tests
# Note:
# - Ignore the boot test, because it requires qemu-system-x86_64 not available on all distributions.
# - Ignore source code tests, which run linters, since we can't ensure that all linters are available
#   and of the same version as in upstream.
# - Explicitly mark btrfs as unsupported on CentOS / RHEL

if [ "${ID}" == "centos" ] || [ "${ID}" == "rhel" ]; then
    UNSUPPORTED_FS="--unsupported-fs btrfs"
fi

TEST_SELECTION_EXPR="not (TestBoot and boot)"

# disable some tests on RHEL-8
if ([ "${ID}" == "rhel" ] || [ "${ID}" == "centos" ]) && [ "${VERSION_ID%%.*}" == "8" ]; then
    # qemu-img info in RHEL-8 produces "raw" as the image format for "vdi" images, which causes tests to fail. Skip it.
    TEST_SELECTION_EXPR="${TEST_SELECTION_EXPR} and not (test_qemu[ext4-vdi] or test_qemu[xfs-vdi])"
    TEST_SELECTION_EXPR="${TEST_SELECTION_EXPR} and not (TestStages and test_qemu)"
fi

sudo python3 -m pytest \
    --rootdir "$(pwd)" \
    --ignore "$(pwd)/test/src" \
    ${UNSUPPORTED_FS:-} \
    -k "${TEST_SELECTION_EXPR}" \
    -v \
    "$(pwd)/test/"
